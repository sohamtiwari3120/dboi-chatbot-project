import re
import traceback
import requests
import pymongo
from pymongo import MongoClient,errors
# import random
# import psycopg2

class SupportBot:
    name = None
    negative_responses = ("nothing", "don't", "stop",
                          "sorry", "nevermind", "nope")
    exit_commands = ("quit", "pause", "exit", "goodbye", "bye",
                     "later", "nothing", "don't", "stop", "no")
    exit_response = "Ok, thank you for visiting us, have a great day!"

    def __init__(self):
        self.matching_phrases = {
            "how_to_pay_bill": [
                r'.*how.*pay bills.*',
                r'.*how.*pay my bill.*',
                r'.*i.*want to pay my bill(s?)'
            ],
            'check_balance': [
                r'.*want.*get.*my.*balance.*account.*number.*is (\d+)',#changed know to get because "no" is substr of "know"
                r'show.*balance'
            ]
        }

    def welcome(self):
        self.name = input("Hi, I'm a customer support representative. Welcome to Deutsche Bank. Before we can help you, may i know your name? \n").strip(
        ).lower().capitalize()

        will_help = input(f"Ok {self.name}, what can I help you with?\n")

        if will_help in self.negative_responses:
            print(self.exit_response)
            return

        self.chatbot_response(will_help)


    def chatbot_response(self, reply):
        while not self.make_exit(reply):
            reply = self.match_reply(reply)


    def make_exit(self, reply):
        for exit_command in self.exit_commands:
            if exit_command in reply:
                print(self.exit_response)
                return True

        return False
    
    def format_reply(self, reply):
        new_reply = ""
        for word in reply.split():
            new_reply+=word+'+'
        return new_reply[:-1]
    
    def match_reply(self, reply):
        for key, values in self.matching_phrases.items():
            for regex_pattern in values:
                found_match = re.match(regex_pattern, reply.lower())

                if found_match:
                    if key == "how_to_pay_bill":
                        return self.how_to_pay_bill_intent()

                    elif key == 'check_balance':
                        print(found_match.groups())
                        print("check balance")
                        if len(found_match.groups()) == 0:
                            return self.check_balance()
                        return self.check_balance(found_match.groups()[0])

        # print(f"https://api.duckduckgo.com/?q={self.format_reply(reply)}&format=json&pretty=1")
        resp = requests.get(f"https://api.duckduckgo.com/?q={self.format_reply(reply)}&format=json&pretty=1")
        try:
            resp = resp.json()
            abstract = resp['Abstract']
            if abstract is None or len(abstract)==0:
                print(resp)
                abstract = f"This link should be helpful: {resp['Results']}"
            print(abstract)
        except Exception as e:
            print(f"Error: {e}")
            print(resp.text)
        return input("\nHope that helps. If not, you can rephrase the question or ask another :)! \n")

    def how_to_pay_bill_intent(self):
        return input(f"Redirecting you to payment interface. Can I  help you with anything else, {self.name}? \n")

    def check_balance(self, account_number=None):
        balance = 0
        if account_number is None:
            account_number = input("Enter Account Number \n")
        try:
            # connection = psycopg2.connect(user="postgres",
            #                               password="6784",
            #                               host="127.0.0.1",
            #                               port="5432",
            #                               database="test")
            # cursor = connection.cursor()
            # postgreSQL_select_Query = "select * from company"

            # # cursor.execute(postgreSQL_select_Query)
            # # account=input("ENTER ACCOUNT \n")
            # cursor.execute(
            #     "SELECT amount FROM company WHERE id= %s",   (account_number,))
            # # print("Selecting rows from company table using cursor.  fetchall")
            # balance = cursor.fetchall()
            # WE CAN DELETE POSTGRESQL PART
            client = connect_to_db()
            db = client.bank  #into bank database
            account_information = db.account_information  #collection named account_information
            account_number = int(account_number)

            item_count = account_information.count_documents({"_id":account_number})
            # check if account number provided is present in Db
            if item_count == 0:
                return input(f"Invalid Account Number Provided. What else can I help you with?\n")
            result = account_information.find_one({"_id":account_number})  #select
            print("Account Details:\nName: "+result["name"]+"\nBalance: "+str(result["balance"]))
            balance = result["balance"]
            # print("Print each row and it's columns values")
            # for row in mobile_records:
            #    print("Id = ", row[0], )
            #    print("Amount = ", row[1])
            #    print("\n")

        except (Exception) as error:
            traceback.print_exc()
            print("Error while fetching balance data: ", error)

        finally:
            # closing database connection.
            # if connection:
            #     cursor.close()
            #     connection.close()
            print("Database connection is closed")

        return input(f"The account with number {account_number} has balance  = {balance} What else can I help you with? \n")


def connect_to_db():
    try:
        client = MongoClient('mongodb+srv://bank_chatbot:bank_chatbot@cluster0.3ylup.mongodb.net/admin?retryWrites=true&w=majority',serverSelectionTimeoutMS=2000)#will try to connect for 2sec
        client.server_info()#check if connected else raise error
    except errors.ServerSelectionTimeoutError as err: 
        print("Unexpected Error!")#becuase connection failed
        # print(err) prints long error string
    return client

if __name__ == '__main__':
    # Create a SupportBot instance
    SupportConversation = SupportBot()
    # Call the .welcome() method
    SupportConversation.welcome()
